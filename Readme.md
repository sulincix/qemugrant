# qemugrant
Vagrant like pratform but uses qemu.

## How to work:
qemugrant downloads vagrant images from vagrantup and extract. Then convert image to qcow2 format. Then remove other files.

## Usage:
```
Usage: qemugrant [options]
Options:
init        : fetch and deploy image
up          : boot image
ssh         : run command in image
halt        : kill image
destroy     : kill and remove image
fixgui      : allow run gui application
box         : remove/add box file
snapshot    : create/restore/delete snapshot
forward     : forward port
```
## Notes:
For X11 forward you must edit sshd_config and install xauth.

For Audio you must run `export PULSE_SERVER=127.0.0.1` in ssh before application.

